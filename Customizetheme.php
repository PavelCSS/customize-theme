<?php
/**
 * Seotoaster 2.0 plugin bootstrap.
 *
 * @todo Add more comments
 * @author Seotoaster core team <core.team@seotoaster.com>
 */

class Customizetheme extends Tools_Plugins_Abstract {

    const CUSTOMIZE_CONFIGURATION_FILE = 'less.ini';

    private $_parser = null;

    private $_themeIniConfig = null;

    private $_configHelper = null;

    private $_themesConfig = null;

    private $_themeName = null;

    private $_themePath = null;

    private $fileContent = null;

    private $cursorPos = null;

    private $strLen = null;

    private $_variableRegx = '/\\G(@[\w-]+)\s*:\s*(@?.[\w-].*);/';

    private $_commentRegx = '/\\G\/\/.*/';

    private $_importRegx = '/\\G(@import)\s*(.[\w\W-].*);/';

	/**
	 * List of action that should be allowed to specific roles
	 *
	 * By default all of actions of your plugin are available to the guest user
	 * @var array
	 */
	protected $_securedActions = array(
		Tools_Security_Acl::ROLE_SUPERADMIN => array(
            'secured'
        )
	);

	/**
	 * Init method.
	 *
	 * Use this method to init your plugin's data and variables
	 * Use this method to init specific helpers, view, etc...
	 */
	protected function _init() {
        $this->_parser = new Less_Parser();
        $this->_configHelper = Zend_Controller_Action_HelperBroker::getStaticHelper('config');
        $this->_themeName = $this->_configHelper->getConfig('currentTheme');
        $this->_themesConfig = Zend_Registry::get('theme');
        $this->_themePath = $this->_websiteHelper->getPath() . $this->_themesConfig['path'] . $this->_themeName . DIRECTORY_SEPARATOR;

        $this->_layout = new Zend_Layout();
        $this->_layout->setLayoutPath(Zend_Layout::getMvcInstance()->getLayoutPath());

        if(($scriptPaths = Zend_Layout::getMvcInstance()->getView()->getScriptPaths()) !== false) {
            $this->_view->setScriptPath($scriptPaths);
        }
        $this->_view->addScriptPath(__DIR__ . '/system/views/');

        $this->_themeIniConfig = $this->_readLessIniFile();
	}

    /**
     * Show customize theme screen
     *
     */
    public function customizeAction() {
        unset($this->_sessionHelper->varRules);
        $varFiles = explode(',', $this->_themeIniConfig['variable-files']);
        $varRules = array();
        foreach($varFiles as $file){
            $file = trim($file, " \n\r");
            $varRules[$file] = $this->_parsingLessFile($this->_themePath . $this->_themeIniConfig['less-path'] . $file);
        }

        $this->_sessionHelper->varRules = $varRules;
        $this->_view->varRules = $varRules;

        $this->_layout->content = $this->_view->render('customize.phtml');
        echo $this->_layout->render();
    }

    /**
     * Show config screen
     *
     */
    public function configAction() {

        if($this->_request->isPost()){

            $themeIniConfig = new Zend_Config(array(
                'less-path' => (isset($this->_requestedParams['less-path']) && !empty($this->_requestedParams['less-path'])) ? $this->_requestedParams['less-path'] : 'css/less/',
                'variable-files' => (isset($this->_requestedParams['variable-files']) && !empty($this->_requestedParams['variable-files'])) ? $this->_requestedParams['variable-files'] : '_getting_started/1-activate_elements.less, _getting_started/1-activate_components.less, _getting_started/1-activate_componentsJS.less, _getting_started/1-activate_toasterWidgets.less, _getting_started/2-variables.less',
                'compiler-files' => (isset($this->_requestedParams['compiler-files']) && !empty($this->_requestedParams['compiler-files'])) ? $this->_requestedParams['compiler-files'] : 'reset.less, nav.less, content.less, style.less, store.less, flexkit.less'
            ), true);

            $iniWriter = new Zend_Config_Writer_Ini(array(
                'config'   => $themeIniConfig,
                'filename' => $this->_themePath . self::CUSTOMIZE_CONFIGURATION_FILE
            ));
            $iniWriter->write();
        }

        $this->_view->configData = $this->_themeIniConfig;

        $this->_layout->content = $this->_view->render('config.phtml');
        echo $this->_layout->render();
    }

    /**
     * Show config screen
     *
     */
    public function saveAction() {
        $varRules = $this->_sessionHelper->varRules;
        $rules = $this->_requestedParams['rules'];
        foreach ($varRules as $name => $file) {
            $content = null;
            foreach ($file as $rule) {
                if ($rule['type'] === 'Comment') {
                    preg_match('/\[(.*)\]/', $rule['value'], $matches);
                    if(!empty($matches)){
                        $content .=  "\n\n";
                    }
                    $content .= $rule['value'] . "\n";
                    continue;
                }
                if ($rule['type'] === 'Variable') {
                    $value = array_shift($rules[$name]);
                    $valuew = !empty($value['value']) ? $value['value'] : $rule['value'];
                    $content .= $rule['name'] . ' : ' . $valuew . ";\n";
                    continue;
                }
                if ($rule['type'] === 'Import') {
                    $content .= $rule['name'] . ' ' . $rule['value'] . ";\n";
                    continue;
                }
            }
            $filePath = $this->_themePath . $this->_themeIniConfig['less-path'] . $name;
            file_put_contents($filePath, $content, LOCK_EX);
        }

        $this->compilerLessFile();
    }

    /**
     * Parsing less files
     *
     */
    public function _readLessIniFile() {
        if(file_exists($this->_themePath . self::CUSTOMIZE_CONFIGURATION_FILE)){
            $configLess = new Zend_Config_Ini($this->_themePath . self::CUSTOMIZE_CONFIGURATION_FILE);
            return $configLess->toArray();
        }
    }

    /**
     * Parsing less files
     *
     */
    public function _parsingLessFile($file_path) {
        $this->fileContent = file_get_contents($file_path);
        $this->cursorPos = 0;
        $this->strLen = strlen($this->fileContent);
        $match = array();
        while($this->cursorPos < $this->strLen){
            $match[] = $this->_parseValue();
        }
        return $match;
    }

    private function _parseValue(){
        $rule = array();
        if( preg_match($this->_commentRegx, $this->fileContent, $match, 0, $this->cursorPos) ){
            $rule['type']  = 'Comment';
            $rule['value'] = $match[0];
        }else if( preg_match($this->_variableRegx, $this->fileContent, $match, 0, $this->cursorPos) ){
            $rule['type']  = 'Variable';
            $rule['name']  = $match[1];
            $rule['value'] = $match[2];
        }else if( preg_match($this->_importRegx, $this->fileContent, $match, 0, $this->cursorPos) ){
            $rule['type']  = 'Import';
            $rule['name']  = $match[1];
            $rule['value'] = $match[2];
        }
        $this->_skipWhitespace(strlen($match[0]));
        return $rule;
    }

    private function _skipWhitespace($length){
        $this->cursorPos += $length;
        for(; $this->cursorPos < $this->strLen; $this->cursorPos++ ){
            $c = $this->fileContent[$this->cursorPos];
            if( ($c !== "\n") && ($c !== "\r") && ($c !== "\t") && ($c !== ' ') ){
                break;
            }
        }
    }

    /**
     * Parsing less files
     *
     */
    public function compilerLessFile() {
        $compilerFiles = explode(',', $this->_themeIniConfig['compiler-files']);

        $options = array( 'cache_dir' => $this->_themePath . 'cache' );
        foreach($compilerFiles as $file){
            $lessFile = $this->_themePath . $this->_themeIniConfig['less-path'] . trim($file);
            $less_files = array( $lessFile => $this->_themePath . $this->_themeIniConfig['less-path'] );
            $files[$lessFile] = $this->_themePath . $this->_themeIniConfig['less-path'];
            try{
                $css_file_name = Less_Cache::Get( $less_files, $options );
                $content = file_get_contents( $this->_themePath . 'cache/' . $css_file_name );
                $fileOut = str_replace('.less', '.css', trim($file));
                file_put_contents($this->_themePath . 'css' . DIRECTORY_SEPARATOR . $fileOut, $content, LOCK_EX);

//                $this->_parser->parseFile($lessFile , $this->_themePath );
//                $content = $this->_parser->getCss();
//                file_put_contents($this->_themePath . 'css' . DIRECTORY_SEPARATOR, $content, LOCK_EX);
            }catch(Exception $e){
                $e->getMessage();
            }
        }
//        $css_file_name = Less_Cache::Get( $files, $options );
//        $compiled = file_get_contents( '/var/www/writable_folder/'.$css_file_name );
    }

	/**
	 * Secured action.
	 *
	 * Will be available to the superadmin only
	 */
	public function securedAction() {

	}
}
